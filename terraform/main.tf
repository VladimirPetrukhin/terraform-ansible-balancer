terraform {
  required_version = "= 1.4.5"
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  token     = ""
  cloud_id  = ""
  folder_id = ""
  zone      = "ru-central1-a"
  
}

# Основной сервер

resource "yandex_compute_instance" "pc" {
  name        = "pc"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd8emvfmfoaordspe1jr"
      size     = 15
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.network-1.id}"
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_vpc_network" "network-1" {
    name        = "network-1"
}

resource "yandex_vpc_subnet" "network-1" {
    name           = "subnet-1"
    v4_cidr_blocks = ["192.168.1.0/24"]
    zone           = "ru-central1-a"
    network_id     = "${yandex_vpc_network.network-1.id}"
}



# Балансировщик
 
variable "folder_id" {
  description = "Folder ID where resources will be created"
  default     = ""
}

resource "yandex_iam_service_account" "ubuntu" {
  name        = "ubuntu"
}

resource "yandex_resourcemanager_folder_iam_member" "editor" {
  folder_id = var.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.ubuntu.id}"
}

resource "yandex_compute_instance_group" "group" {
  name               = "group"
  folder_id          = var.folder_id
  service_account_id = "${yandex_iam_service_account.ubuntu.id}"
  instance_template {
    platform_id = "standard-v3"
    resources {
      memory        = 4
      cores         = 2
    }


    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = "fd8evlqsgg4e81rbdkn7"
        type     = "network-hdd"
        size     = 15
      }
    }


    network_interface {
      network_id = "${yandex_vpc_network.network-2.id}"
      subnet_ids = ["${yandex_vpc_subnet.subnet-1.id}", "${yandex_vpc_subnet.subnet-2.id}"] 
      nat        = true
    }

    metadata = {
      ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
      user-data = "${file("user_data.sh")}"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    zones = ["ru-central1-a", "ru-central1-b"]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name = "balancer"
  }
}

resource "yandex_lb_network_load_balancer" "balancer" {
  name = "balancer"
  listener {
    name = "balanc-listener"
    port = 80
  }
  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.group.load_balancer.0.target_group_id}"
    healthcheck {
      name                = "health-check"
      unhealthy_threshold = 5
      healthy_threshold   = 5
      http_options {
        port = 80
      }
    }
  }
}

resource "yandex_vpc_network" "network-2" {
  name = "network-balancer"
}


resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet-2"
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.network-2.id}"
  v4_cidr_blocks = ["192.168.2.0/24"]
}

resource "yandex_vpc_subnet" "subnet-2" {
  name           = "subnet-3"
  zone           = "ru-central1-b"
  network_id     = "${yandex_vpc_network.network-2.id}"
  v4_cidr_blocks = ["192.168.3.0/24"]
}



output "instance_external_ip" {
  value = yandex_compute_instance.pc.network_interface.0.nat_ip_address
}
